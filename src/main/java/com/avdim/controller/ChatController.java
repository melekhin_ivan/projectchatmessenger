package com.avdim.controller;

import com.avdim.model.Account;
import com.avdim.model.ChatMessage;
import com.avdim.services.AdminService;
import com.avdim.services.ChatService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

/**
 * Контроллер для обработки сообщений
 * Отвечает за работу с пользователями и их активностью.
 */
@Controller
public class ChatController {

    /**
     * Приватное поле внедряемое через конструктор класса {@link ChatService}
     */
    private final ChatService chatService;

    /**
     * Приватное поле внедряемое через конструктор класса {@link AdminService}
     */
    private final AdminService adminService;

    /**
     * Конструктор класса
     *
     * @param chatService - внедряемый как зависимость класс
     * @param adminService - внедряемый как зависимость класс
     */
    public ChatController(ChatService chatService, AdminService adminService) {

        this.chatService = chatService;
        this.adminService = adminService;
    }

    /**
     * Метод для получения и отправки сообщения от клиента
     *
     * @param chatMessage - модель сообщения     *
     * @return сообщение
     */
    @MessageMapping("/{id}/chat.sendMessage")
    @SendTo("/topic/{id}/public")
    private ChatMessage sendMessageRoom(@Payload ChatMessage chatMessage) {
        chatService.saveMessageToHistory(chatMessage);
        return chatMessage;
    }

    /**
     * Метод для подключения пользователя в выбранную комнату и
     * публикации сообщения об подключении к чату
     *
     * @param chatMessage - модель сообщения
     * @param headerAccessor - экземпляр класса для работы с заголовками сообщений
     * @return сообщение
     */
    @MessageMapping("/{id}/chat.addUser")
    @SendTo("/topic/{id}/public")
    public ChatMessage addUserRoom(@Payload ChatMessage chatMessage,
                                   SimpMessageHeaderAccessor headerAccessor) {
        Objects.requireNonNull(headerAccessor.getSessionAttributes()).put("username", chatMessage.getSender());
        Objects.requireNonNull(headerAccessor.getSessionAttributes()).put("room", chatMessage.getRoom());
        return chatMessage;
    }

    /**
     * Метод POST для обработки запроса формы регистрации нового пользовател
     *
     * @param accountBody - данные аккаунта пользователя
     * @return статус
     */
    @RequestMapping(value = "/chat.registryUser", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> registerUser(@RequestBody Account accountBody) {
        boolean result = chatService.createAccount(accountBody);
        if (result) {
            return new ResponseEntity<>("Successful registration", HttpStatus.OK);
        }
        return new ResponseEntity<>("Registration failed", HttpStatus.CONFLICT);
    }

    /**
     * Данный POST метод отвечает за логирование пользователя
     *
     * @param loginAccount - данные аккаунта пользователя
     * @param id - номер комнаты чата
     * @param response - статус
     * @return статус
     */
    @RequestMapping(value = "/{id}/chat.loginUser", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> loginToChat(@RequestBody Account loginAccount,
                                              @PathVariable String id,
                                              HttpServletResponse response) {
        String result = chatService.loginToChat(loginAccount, id);
        String str;
        HttpStatus status;
        switch (result) {
            case "ADMIN": {
                Cookie cookie = adminService.getCookieAdmin();
                response.addCookie(cookie);
                str = "{\"IN\":\"ADMIN\"}";
                status = HttpStatus.OK;
                break;
            }
            case "BLOCK": {
                str = "{\"IN\":\"BLOCK\"}";
                status = HttpStatus.OK;
                break;
            }
            case "OK": {
                str = "{\"IN\":\"OK\"}";
                status = HttpStatus.OK;
                break;
            }
            case "FAIL": {
                str = "{\"IN\":\"FAIL\"}";
                status = HttpStatus.OK;
                break;
            }
            case "CLASH": {
                str = "{\"IN\":\"CLASH\"}";
                status = HttpStatus.OK;
                break;
            }
            default: {
                str = "{\"IN\":\"UNAVAILABLE\"}";
                status = HttpStatus.SERVICE_UNAVAILABLE;
            }
        }
        return new ResponseEntity<>(str, status);
    }

    /**
     * Метод GET, отвечающий за передачу количества пользователей онлайн, находящихся в конкретной комнате чата
     *
     * @param id - номер комнаты чата
     * @return количество пользователей онлайн и статус
     */
    @RequestMapping(value = "/{id}/chat.onlineUsers", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TreeSet<String>> onlineUser(@PathVariable String id) {
        TreeSet<String> result = chatService.onlineUser(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Метод GET для подгрузки истории сообщений в чате
     *
     * @param id - номер комнаты чата
     * @return установленное в соответсвующем классе количество сообщений и статус
     */
    @RequestMapping(value = "/{id}/chat.history", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<ChatMessage>> loadHistory(@PathVariable String id) {
        List<ChatMessage> historyUsers = chatService.loadHistory(id);
        return new ResponseEntity<>(historyUsers, HttpStatus.OK);
    }
}
