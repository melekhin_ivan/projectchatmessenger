package com.avdim.controller;

import com.avdim.services.AdminService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Класс AdminController - содержит методы для обработки запросов
 * связанных с администрированием пользователей на доступ в чат
 * сдержит приватное поле внедряемого класса {@link AdminService}
 */
@Controller
public class AdminController {
    /**
     * Приватное поле внедряемого через конструктор класса {@link AdminService}
     */
    private final AdminService adminService;

    /**
     * Конструктор класса
     *
     * @param adminService - внедряемый как зависимость класс
     */
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }


    /**
     * Метод обрабатывает запросы поступающие по соответствующему url /admin
     *
     * @param request - содержит данные запроса от клиента
     * @return - возвращает ответ содержащий разрешение или запрет на доступ к странице алминистратора
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> adminPanel(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        boolean isAdmin = adminService.checkAdminCookie(cookies);
        if (isAdmin) {
            String adminPage = adminService.getAdminPage();
            return new ResponseEntity<>(adminPage, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    /**
     * Метод обрабатывает запросы поступающие для выполнения блокировки
     * или разблокировки пользователя
     *
     * @param name   - параметр содержит ник пользователя
     * @param action - параметр содержит значение действия необходимого
     *               выполнить, заблокировать или разблокировать
     * @return - возвращает результат выполненого действия
     */
    @RequestMapping(value = "/admin/blocked", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> blockedUser(HttpServletRequest request,
                                              @RequestParam String name,
                                              @RequestParam String action) {
        Cookie[] cookies = request.getCookies();
        boolean isAdmin = adminService.checkAdminCookie(cookies);
        if (isAdmin) {
            String resulAction = adminService.actionBlocked(name, action);
            return new ResponseEntity<>(resulAction, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
}
