package com.avdim.model;

import java.io.Serializable;

/**
 * Класс, содержащий учетные данные пользователя чата, методы для доступа к приватным полям
 */
public class Account implements Serializable {

    /**
     * Поле имя аккаунта
     */
    private String accountName;

    /**
     * Поле никнейм аккаунта
     */
    private String accountNickName;

    /**
     * Поле пароль аккаунта
     */
    private String accountPassword;

    /**
     * Конструктор, создающий экземпляр класса, присваивает полям экземпляра значения передаваемых параметров
     *
     * @param name - содержит имя аккаунта создаваемого экземпляра
     * @param nick - содержит никнейм аккаунта создаваемого экземпляра
     * @param password - содержит пароль аккаунта создаваемого экземпляра
     */
    public Account(String name, String nick, String password) {
        this.accountNickName = nick;
        this.accountPassword = password;
        this.accountName = name;
    }

    /**
     * Метод возвращает имя аккаунта
     *
     * @return accountName - содержит имя аккаунта
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Метод изменения имени аккаунта
     *
     * @param accountName - содержит новое имя аккаунта
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * Метод возвращает никнейм аккаунта
     *
     * @return accountNickName - содержит никнейм аккаунта
     */
    public String getAccountNickName() {
        return accountNickName;
    }

    /**
     * Метод изменения никнейма аккаунта
     *
     * @param accountNickName - содержит новый никнейм аккаунта
     */
    public void setAccountNickName(String accountNickName) {
        this.accountNickName = accountNickName;
    }

    /**
     * Метод возвращает пароль аккаунта
     *
     * @return accountPassword - содержит пароль аккаунта
     */
    public String getAccountPassword() {
        return accountPassword;
    }

    /**
     * Метод изменения пароля аккаунта
     *
     * @param accountPassword - содержит новый пароль аккаунта
     */
    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

}
