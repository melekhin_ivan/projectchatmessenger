package com.avdim.model;

import java.io.Serializable;

/**
 * Класс, содержащий данные сообщения, методы для доступа к приватным полям
 */
public class ChatMessage implements Serializable {

    /**
     * Поле тип сообщения
     */
    private MessageType type;

    /**
     * Поле содержимое
     */
    private String content;

    /**
     * Поле отправитель
     */
    private String sender;

    /**
     * Поле название комнаты
     */
    private String room;

    /**
     * Поле временная метка
     */
    private String timestamp;

    /**
     * Объявление перечисления, в котором указан список констант (CHAT, HISTORY, JOIN, LEAVE)
     */
    public enum MessageType {
        CHAT,
        HISTORY,
        JOIN,
        LEAVE
    }

    /**
     * Метод возвращает тип сообщения
     *
     * @return type - содержит тип сообщения
     */
    public MessageType getType() {
        return type;
    }

    /**
     * Метод устанавливает тип сообщения
     *
     * @param type - содержит новый тип сообщения
     */
    public void setType(MessageType type) {
        this.type = type;
    }

    /**
     * Метод возвращает содержимое сообщения
     *
     * @return content - содержит сообщение
     */
    public String getContent() {
        return content;
    }

    /**
     * Метод устанавливает содержимое сообщения
     *
     * @param content - содержит новое сообщение
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Метод возвращает отправителя
     *
     * @return sender - содержит отправителя
     */
    public String getSender() {
        return sender;
    }

    /**
     * Метод устанавливает отправителя
     *
     * @param sender - содержит нового отправителя
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * Метод получения временной метки
     *
     * @return timestamp - содержит временную метку
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Метод устанавливает временную метку
     *
     * @param timestamp - содержит новую временную метку
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Метод возвращает названия комнаты
     *
     * @return room - содержит название комнаты
     */
    public String getRoom() {
        return room;
    }

    /**
     * Метод устанавливает название комнаты
     *
     * @param room - содержит новое название комнаты
     */
    public void setRoom(String room) {
        this.room = room;
    }

}
