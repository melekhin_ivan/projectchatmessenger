package com.avdim.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AccountTests {

    /**
     * Создается объект типа Account
     */
    Account account = new Account("Ivan", "V_A_N_O", "12345");

    /**
     * Тест проверяет работу метода {@link Account#getAccountName()}
     */
    @Test
    public void getAccountName_Test() {
        String result = account.getAccountName();
        Assertions.assertEquals("Ivan", result);
    }

    /**
     * Тест проверяет работу метода {@link Account#setAccountName(String)}
     */
    @Test
    public void setAccountName_Test() {
        account.setAccountName("Petr");
        Assertions.assertEquals("Petr", account.getAccountName());
    }

    /**
     * Тест проверяет работу метода {@link Account#getAccountNickName()}
     */
    @Test
    public void getAccountNickName_Test() {
        String result = account.getAccountNickName();
        Assertions.assertEquals("V_A_N_O", result);
    }

    /**
     * Тест проверяет работу метода {@link Account#setAccountNickName(String)}
     */
    @Test
    public void setAccountNickName_Test() {
        account.setAccountNickName("P_E_T_R_O");
        Assertions.assertEquals("P_E_T_R_O", account.getAccountNickName());
    }

    /**
     * Тест проверяет работу метода {@link Account#getAccountPassword()}
     */
    @Test
    public void getAccountPassword_Test() {
        String result = account.getAccountPassword();
        Assertions.assertEquals("12345", result);
    }

    /**
     * Тест проверяет работу метода {@link Account#setAccountPassword(String)}
     */
    @Test
    public void setAccountPassword_Test() {
        account.setAccountPassword("67891");
        Assertions.assertEquals("67891", account.getAccountPassword());
    }
}
