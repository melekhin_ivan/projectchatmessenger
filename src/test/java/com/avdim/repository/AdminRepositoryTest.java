package com.avdim.repository;

import com.avdim.repository.AdminRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import java.sql.*;

/**
 * Тесты для тестирования {@link AdminRepository}
 */
@SpringBootTest
public class AdminRepositoryTest {

    private String urlToDatabase;

    @Value("${property.url.database}")
    public void setUrlToDatabase(String urlToDatabase) {
        this.urlToDatabase = urlToDatabase;
    }

    @Autowired
    AdminRepository adminRepository;

    /**
     * Тестирование метода {@link AdminRepository#actionBlockUnblock(String, String)}
     */
    @Test
    public void actionBlockUnblock_Test() {
        final String sqlDelete = "delete from blocked_users where user = 'TestBlock'";
        int result = adminRepository.actionBlockUnblock("TestBlock", "blocked");
        Assertions.assertTrue(result == 1);

        int resultDuplicate = adminRepository.actionBlockUnblock("TestBlock", "blocked");
        Assertions.assertTrue(resultDuplicate == 0);

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement()){
            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Тестирование метода {@link AdminRepository#actionBlockUnblock(String, String)}
     */
    @Test
    public void actionBlockUnblockFind_Test() {
        final String sql = "select * from blocked_users where user = 'TestBlock'";
        final String sqlDelete = "delete from blocked_users where user = 'TestBlock'";
        adminRepository.actionBlockUnblock("TestBlock", "blocked");

        try(Connection connection = DriverManager.getConnection(urlToDatabase);
            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY)){
            ResultSet resultSet = statement.executeQuery(sql);

            String result = resultSet.getString("user");
            Assertions.assertTrue("TestBlock".equals(result));

            statement.executeUpdate(sqlDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
