package com.avdim.controller;

import com.avdim.model.Account;
import com.avdim.services.AdminService;
import com.avdim.services.ChatService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.Cookie;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тестирование класса {@link ChatController}
 */
@WebMvcTest(ChatController.class)
@TestPropertySource(locations = "/application.properties")
@AutoConfigureMockMvc
public class ChatControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    AdminService adminService;

    @MockBean
    ChatService chatService;


    /**
     * Тестирование метода подсчёта пользователей онлайн
     */
    @Test
    public void onlineUser_test() throws Exception {

        TreeMap<String, Set<String>> usersToRoom = new TreeMap<>();

        TreeSet<String> treSeUsers = new TreeSet<>();
        treSeUsers.add("test1");

        usersToRoom.put("1", treSeUsers);
        usersToRoom.get("1").add("test2");

        TreeSet<String> result = (TreeSet<String>) usersToRoom.get("1");

        Mockito.when(chatService.onlineUser("1")).thenReturn(result);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/1/chat.onlineUsers")
                .contentType(MediaType.ALL))
                .andExpect(content().json(mapper.writeValueAsString(result)))
                .andExpect(status().isOk());
    }

    /**
     * Тестирование метода логирования в чат, в случае если пользователь не заблокирован
     */
    @Test
    public void loginToChat_success_test() throws Exception {

        Account account = new Account("test_name", "test_nick", "test_pass");

        String result = "OK";
        Mockito.doReturn(result).when(chatService).loginToChat(Mockito.any(), Mockito.any());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/1/chat.loginUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"IN\":\"OK\"}"));
    }

    /**
     * Тестирование метода логирования в чат, в случае если пользователь был заблокирован администратором
     */
    @Test
    public void loginToChat_block_test() throws Exception {

        Account account = new Account("test_name", "test_nick", "test_pass");

        String result = "BLOCK";
        Mockito.doReturn(result).when(chatService).loginToChat(Mockito.any(), Mockito.any());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/1/chat.loginUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"IN\":\"BLOCK\"}"));
    }

    /**
     * Тестирование метода логирования в чат, в случае неверного результата
     */
    @Test
    public void loginToChat_unavailableResult_test() throws Exception {

        Account account = new Account("test_name", "test_nick", "test_pass");

        String result = "NO_SUCH_RESULT";
        Mockito.doReturn(result).when(chatService).loginToChat(Mockito.any(), Mockito.any());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/1/chat.loginUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(account)))
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string("{\"IN\":\"UNAVAILABLE\"}"));
    }

    /**
     * Тестирование метода логирования в чат, в случае неверного результата
     */
    @Test
    public void loginToChat_Admin_test() throws Exception {

        Account account = new Account("test_name", "test_nick", "test_pass");

        String result = "NO_SUCH_RESULT";
        Mockito.doReturn(result).when(chatService).loginToChat(Mockito.any(), Mockito.any());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/1/chat.loginUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(account)))
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string("{\"IN\":\"UNAVAILABLE\"}"));
    }

    /**
     * Тестирование метода логирования в чат, с использованием cookie для логирования администратора
     */
    @Test
    public void loginToChat_cookie_test() throws Exception {

        Account account = new Account("test_name", "test_nick", "test_pass");

        String result = "ADMIN";
        Mockito.when(chatService.loginToChat(Mockito.any(Account.class), Mockito.anyString())).thenReturn(result);
        Mockito.when(adminService.getCookieAdmin()).thenReturn(Mockito.mock(Cookie.class));

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/1/chat.loginUser")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(account)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"IN\":\"ADMIN\"}"));
    }
}
